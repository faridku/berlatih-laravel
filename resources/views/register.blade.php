<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>form.html</title>
</head>

<body>
  <h1>Buat Account Baru!</h1>
  <h3>Sign Up Form</h3>
  <form action="/welcome" method="post">
    @csrf
    <label for="firstname">First Name:</label><br><br>
    <input type="text" name="firstname" id="firstname"><br><br>
    <label for="lastname">Last Name:</label><br><br>
    <input type="text" name="lastname" id="lastname"><br><br>

    <label for="Gender">Gender:</label><br><br>
    <input type="radio" id="pria" name="gender" value="male">
    <label for="pria">Pria</label><br>
    <input type="radio" id="wanita" name="gender" value="female">
    <label for="wanita">Wanita</label><br><br>

    <label for="nation">Nationality:</label><br><br>
    <select name="nation" id="nation">
      <option value="indonesian">Indonesian</option>
      <option value="malaysian">Malaysian</option>
      <option value="australian">Australian</option>
    </select><br><br>

    <label for="language">Language spoken:</label><br><br>
    <input type="checkbox" id="language1" name="language" value="indonesia">
    <label for="language1">Bahasa Indonesia</label><br>
    <input type="checkbox" id="language2" name="language" value="english">
    <label for="language2">English</label><br>
    <input type="checkbox" id="language3" name="language" value="other">
    <label for="language3">Other</label><br><br>

    <label for="bio">Bio:</label><br><br>
    <textarea id="bio" rows="10" cols="40"></textarea><br>
    <input type="submit" value="Sign Up">
  </form>

</body>

</html>